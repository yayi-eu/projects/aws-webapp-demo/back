var express = require('express');
var router = express.Router();

router.get('/', function (req, res) {
    let responseCode = 200;
    let responseBody = [
        {
            comment: "Hello name  : comment 1",
            id: "1"
        },
        {
            comment: "Hello comments 2",
            id: "2"
        },
        {
            comment: "Hello World 3 ",
            id: "3"
        },
    ];


    res.type('application/json')
    res.set('Access-Control-Allow-Origin','*')
    res.set('x-custom-header','my custom header value')
    res.send(JSON.stringify(responseBody))
})
module.exports = router;
