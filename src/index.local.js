var commentsRouter = require('./routes/comments');

const express = require('express')
const app = express()
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use('/comments', commentsRouter);

app.listen(3001, function () {
    console.log('Example app listening on port 3001!')
})
